/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2015  <copyright holder> <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DUPLICATEFINDER_H
#define DUPLICATEFINDER_H

#include <vector>
#include <boost/container/flat_map.hpp>

#include <QObject>
#include <QString>

#include "duplicatemodel.h"

using hash_t = unsigned long long;
using filepath = QString;

class DuplicateFinder : public QObject
{
  Q_OBJECT
  
public:
  DuplicateFinder(DuplicateModel& m, QObject* parent = 0);
  const static qint64 LARGE_FILE = 1024*1024*128; // 128 MB TODO: this should depend on RAM availability
  
private:
  DuplicateModel& model;
  boost::container::flat_map<qint64, std::vector<QString>> collectSameSizeFiles(const filepath& initialDirectory);
  void addDuplicates(const std::vector<QString>& paths);

public slots:
  void findPossibleDuplicatesInDir(filepath initialDirectory);  
    
signals:
  void foundPossibleDuplicates(int duplicatedCount);
  void processedDuplicates(int processedCount);
  void processedAllFiles();
};

#endif // DUPLICATEFINDER_H
