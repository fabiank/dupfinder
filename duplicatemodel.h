/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2015  <copyright holder> <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DUPLICATEMODEL_H
#define DUPLICATEMODEL_H
#include <vector>

#include <QMutex>
#include <QHash>
#include <QByteArray>
#include <QAbstractItemModel> 

class DuplicateModel : public  QAbstractItemModel
{
  Q_OBJECT
  
public:
    DuplicateModel(QObject* parent = 0);
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE  Q_DECL_FINAL;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE  Q_DECL_FINAL;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE  Q_DECL_FINAL;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE  Q_DECL_FINAL;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE  Q_DECL_FINAL;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE  Q_DECL_FINAL;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE  Q_DECL_FINAL;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE Q_DECL_FINAL;
    void addDuplicatedFiles(std::vector<QString> files);
    void addMultipleDuplicatedFiles(std::vector<std::vector<QString>> files);
private:
    mutable QMutex modelLock; // protects addDuplicatedFiles
    std::vector<std::vector<QString>> duplicatedFiles;
};

#endif // DUPLICATEMODEL_H
