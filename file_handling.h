#ifndef FILE_HANDLING_H
#define FILE_HANDLING_H

#include <QFile>
#include <memory>

class FileChunk {
  private:
    uchar* data;
    qint64 size;
    QFile* file;
  public:
    FileChunk(QFile* file_, qint64 offset, qint64 size_);

    bool operator==(const FileChunk& other);

   FileChunk(const FileChunk&) = delete;
   FileChunk operator=(const FileChunk& other) = delete;

   FileChunk(FileChunk&& other);

   FileChunk& operator=(FileChunk&& other );

    ~FileChunk();
};


class FileIterator {
  private:
    std::shared_ptr<QFile> fileptr;
    qint64 offset;
    qint64 fileSize;
    static constexpr qint64 chunkSize = 4*4096;

    qint64 getChunkSize();

  public:
    FileIterator(const QString& path);

    QString getPath();

    FileChunk getChunk();

    /**
     * @returns: true iff offset is still within bounds of the file
     */
    bool advance();
};

#endif // FILE_HANDLING_H

