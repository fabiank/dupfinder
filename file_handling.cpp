#include "file_handling.h"

FileChunk::FileChunk(QFile* file_, qint64 offset, qint64 size_): size(size_), file(file_) {
    data = file->map(offset, size);
}
bool FileChunk::operator==(const FileChunk& other) {
    Q_ASSERT(this->size == other.size);
    auto result = (memcmp(this->data, other.data, this->size) == 0);
    return result;
}
FileChunk::FileChunk(FileChunk&& other): data(other.data), size(other.size) ,file(other.file)  {
    other.size = 0;
    other.data = nullptr;
}
FileChunk& FileChunk::operator=(FileChunk&& other) {
    this->data = other.data;
    this->size = other.size;
    this->file = other.file;
    other.size = 0;
    other.data = nullptr;
    return *this;
}
FileChunk::~FileChunk() {
    if (data != nullptr) {
        file->unmap(data);
    }
}
qint64 FileIterator::getChunkSize() {
    if (offset + chunkSize < fileSize)
        return chunkSize;
    else
        return fileSize - offset;
}
FileIterator::FileIterator(const QString& path): fileptr(new QFile(path)), offset(0) {
    if (!fileptr->open(QIODevice::ReadOnly)) {
        throw std::invalid_argument("TODO: error msg");
    }
    fileSize = fileptr->size();
}
QString FileIterator::getPath() {
    return fileptr->fileName();
}
FileChunk FileIterator::getChunk() {
    return FileChunk(this->fileptr.get(), this->offset, this->getChunkSize());
}
bool FileIterator::advance() {
    offset += chunkSize;
    return offset < fileSize;
}
