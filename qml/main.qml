import QtQuick 2.2
import QtQml.Models 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0



ApplicationWindow {
    title: "My Application"
    width: 640
    height: 480
    visible: true

    
    FileDialog {
      id: folderChooser
      title: "Please choose a folder"
      folder: shortcuts.home
      selectFolder: true
      onAccepted: {
        console.log("You chose: " + fileDialog.fileUrls)
      }
      onRejected: {
        console.log("Canceled")
      }
    }
    
    DelegateModel {
      id: dm
      model: myModel
              delegate: Rectangle {
            width: 200; height: 25
            Text { text: display }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (model.hasModelChildren)
                        view.model.rootIndex = view.model.modelIndex(index)
                }
            }
        }
    }
 
    Row {

    Button {
      onClicked: {folderChooser.open()}
      text: "Testbutton"
    }
    
    Label {
      text: folderChooser.fileUrl === "" ? folderChooser.fileUrl + " was selected" : "no folder selected so far"
    }
    
    }
    
    Rectangle {
      id: foo
      anchors.centerIn: parent
      width: 0.9*parent.width
      height: 0.9*parent.height
      border.width: 1
      border.color: "blue"

      Row {
      
                ListView {
      id: view1
      width: 300
      height: 400
      model: myModel
    }
    
                    ListView {
      id: view
      width: 300
      height: 400
      model: dm
    }
    
      }
      
      /*TreeView {
       anchors.fill: parent
        TableViewColumn {
          title: "Name"
          role: "display"
          width: 500
        }
        model: myModel

      }*/
    }
    
}