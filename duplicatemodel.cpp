/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2015  <copyright holder> <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "duplicatemodel.h"
#include <QVariant>
#include <QMutexLocker>

constexpr quintptr isTop = (quintptr)-1;

int DuplicateModel::columnCount(const QModelIndex& parent) const
{
  return 1;
}


int DuplicateModel::rowCount(const QModelIndex& parent) const {
  QMutexLocker lock(&modelLock);
  if (!parent.isValid())
    return duplicatedFiles.size();
  else if (parent.internalId() == isTop)
    return duplicatedFiles.at(parent.row()).size();
  else
    return 0;
}

QModelIndex DuplicateModel::index ( int row, int column, const QModelIndex& parent ) const
{
    if (!hasIndex(row, column, parent)) {
      return QModelIndex();
    }
    if (!parent.isValid()) {
      // topmost
      return createIndex(row, column, isTop);
    } else {
      return createIndex(row, column, parent.row() );
    }
}


QVariant DuplicateModel::data(const QModelIndex& index, int role) const {
  QMutexLocker lock(&modelLock);
  if (role != Qt::DisplayRole)
    return QVariant();
  if (!index.isValid())
    return QVariant();
  if (index.internalId() == isTop) {
    if (index.row() < duplicatedFiles.size())
      return duplicatedFiles.at(index.row()).front();
    else
      return QVariant();
  } else {
    const std::vector<QString>& duplicates = duplicatedFiles.at(index.internalId());
    if (index.row() >= duplicates.size())
      return QVariant();
    return QVariant(duplicates.at(index.row()));
  }
}

Qt::ItemFlags DuplicateModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
      return 0;
    return QAbstractItemModel::flags(index);
}

QModelIndex DuplicateModel::parent(const QModelIndex& index) const
{
  if (!index.isValid() || index.internalId() == isTop)
    return QModelIndex();
  return createIndex(index.internalId(), 0, isTop);
}



QVariant DuplicateModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    return QVariant();
}

QHash<int, QByteArray> DuplicateModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Qt::DisplayRole] = "display";
    return roles;
}

void DuplicateModel::addDuplicatedFiles(std::vector< QString > files) {
  QMutexLocker lock(&modelLock);
  this->beginInsertRows(QModelIndex {}, duplicatedFiles.size(), duplicatedFiles.size()+1);
  duplicatedFiles.push_back(files);
  this->endInsertRows();
}

DuplicateModel::DuplicateModel(QObject* parent): QAbstractItemModel(parent), modelLock(QMutex::Recursive) {}

void DuplicateModel::addMultipleDuplicatedFiles(std::vector< std::vector< QString > > files) {
    QMutexLocker lock(&modelLock);
    this->beginInsertRows(QModelIndex {}, duplicatedFiles.size(), duplicatedFiles.size() + files.size());
    duplicatedFiles.insert(duplicatedFiles.end(), files.begin(), files.end());
    this->endInsertRows();
}
