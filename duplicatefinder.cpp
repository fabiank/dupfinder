/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2015  <copyright holder> <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "duplicatefinder.h"
#include "file_handling.h"
#include <xxHash/xxhash.h>
#include <cppitertools/grouper.hpp>

#include <algorithm>
#include <memory>
#include <string.h>

#include <QDirIterator>
#include <QFile>
#include <QtGlobal>
#include <QMutexLocker>
namespace QtConcurrent {
  
inline bool selectIteration(std::input_iterator_tag) {
  return false;
}

}

#include <QtConcurrent>
#include <QDebug>
#include <QAtomicInt>

// taken from http://stackoverflow.com/questions/800955/remove-if-equivalent-for-stdmap
template< typename ContainerT, typename PredicateT >
  void erase_if( ContainerT& items, const PredicateT& predicate ) {
    for( auto it = items.begin(); it != items.end(); ) {
      if( predicate(*it) ) 
        it = items.erase(it);
      else
        ++it;
    }
};


/**
 * @param filepath: path to a file
 * @param hash2conflictingPaths: a mapping from hashes to file paths with the same hash
 * @short Hashes the file found at fpath and updates hash2conflictingPaths accordingly
 */
void hashAndCheck(const filepath & fpath, boost::container::flat_map<hash_t, std::vector<QString>> & hash2conflictingPaths) {
      QFile f {fpath};
      if (!f.open(QIODevice::ReadOnly)) {
        // log error?
        return;
      }  
      QByteArray rawData = f.readAll();
      const char* rawData_ = rawData.constData();
      size_t rawDataSize = rawData.size();
      auto hash = XXH64(rawData_, rawDataSize, 0);
      auto pos = hash2conflictingPaths.find(hash);
      if (pos == hash2conflictingPaths.end()) {
        // first time we've seen the element
        hash2conflictingPaths[hash].push_back(fpath);
      } else {
        hash2conflictingPaths.at(hash).push_back(fpath);
  }
}

std::vector<std::vector<QString>> partitionByFileContent(const std::vector<QString>& paths) {
  std::vector<std::vector<QString>> result {};
  std::vector<std::vector<FileIterator>> working_queue {};
  std::vector<FileIterator> fiterators {};
  fiterators.reserve(paths.size());
  std::transform(paths.cbegin(), paths.cend(), std::back_inserter(fiterators), [](const QString& path) {
    FileIterator fiter {path};
    return fiter;
  });  
  working_queue.push_back(fiterators);
  do {
    std::vector<FileIterator> current = working_queue.back();
    working_queue.pop_back();
    std::vector<FileIterator> equal;
    std::vector<FileIterator> different;
    FileChunk first_data = current.front().getChunk();
    equal.push_back(current.front());
    std::partition_copy(std::next(current.begin()), current.end(),
                                  std::back_inserter(equal), std::back_inserter(different), [&first_data](FileIterator fiter) {
                                    return fiter.getChunk() == first_data;
                                  });
    // when processing equal and different, we only need to consider vectors with size >= 2
    // as everything else would be a unicum
    // handle equal part
    if (equal.size() >= 2) {
      bool unfinished = true;
      for (auto& fiter: equal) {
        unfinished = fiter.advance();
      }
      if (unfinished)
        working_queue.push_back(equal);
      else {
        std::vector<QString> paths;
        paths.reserve(equal.size());
        std::transform(equal.begin(), equal.end(), std::back_inserter(paths), [](FileIterator fiter) {
          return fiter.getPath();}
        );
	result.push_back(paths);
      }
    }
    // handle different part
    if (different.size() >= 2) {
      // all we know is that the data is different than the first element of current
      // but we do not know how the data relates between elements in different
      // therefore it would be counterproductive to advance
      working_queue.push_back(different);
    }
  } while(!working_queue.empty());
  return result;
}


boost::container::flat_map<qint64, std::vector<QString>>  DuplicateFinder::collectSameSizeFiles(const filepath& initialDirectory) {
  decltype(collectSameSizeFiles(initialDirectory)) size2possibleDuplicates {};
  // step 1: collect all files and store a mapping from size to path
  QDirIterator dirit {initialDirectory, QDirIterator::FollowSymlinks | QDirIterator::Subdirectories};
  int counter = 0;
  while (dirit.hasNext()) {
    ++counter;
    if (counter ==100) {
      counter = 0;
      if ( QThread::currentThread()->isInterruptionRequested() ) {
        break;
      }
    }
    filepath fpath = dirit.next();
    QFileInfo finfo = dirit.fileInfo();
    if (finfo.isSymLink() || !finfo.isFile() || finfo.size() == 0) {
      // remember empty files?
      continue;
    }
    size2possibleDuplicates[finfo.size()].push_back(fpath);
  }
  // step 2: clear all file sizes which are unique
  erase_if(size2possibleDuplicates, [](std::pair<qint64, std::vector<QString>>& sizePathsPair) {
    return sizePathsPair.second.size() == 1; // cannot be smaller than 1
  });
  emit foundPossibleDuplicates(size2possibleDuplicates.size());
  return size2possibleDuplicates;
}

void DuplicateFinder::addDuplicates(const std::vector< QString >& paths)
{
  for (const auto &equal_files: partitionByFileContent(paths)) {
    {
      model.addDuplicatedFiles(equal_files);
    }
  }
}

static void collectDuplicatesInAccu(std::vector< std::vector< QString > >& duplicateAccumulator, const std::vector< QString >& potentialDuplicates)
{
  for (const auto &equal_files: partitionByFileContent(potentialDuplicates)) {
    {
      duplicateAccumulator.push_back(equal_files);
    }
  }
}

template<class InputIt, class UnaryFunction, class Predicate>
void if_then_else(InputIt first, InputIt last, Predicate condition,  UnaryFunction consequence, UnaryFunction alternative)
{
    std::for_each(first, last, [&]( decltype(*first)& elem ){
      if (condition(elem))
        consequence(elem);
      else
        alternative(elem);
    });
}


static void processElements(std::vector< std::vector< QString > >& duplicateAccumulator,  std::pair<const qint64, std::vector<QString>> sizePathsPair) {
    Q_ASSERT(sizePathsPair.second.size() > 1); // there is actually a chance of finding a duplicate
    if (sizePathsPair.first > DuplicateFinder::LARGE_FILE) {
      collectDuplicatesInAccu(duplicateAccumulator, sizePathsPair.second);
    } else {
      if (sizePathsPair.second.size() > 2) { // use hashing for further disambiguation
        boost::container::flat_map<hash_t, std::vector<QString>> hash2conflictingPaths;
        for (const auto& path: sizePathsPair.second)
          hashAndCheck(path, hash2conflictingPaths);
        for (const auto& hashPathPair: hash2conflictingPaths)
          if (hashPathPair.second.size() >= 2)
            collectDuplicatesInAccu(duplicateAccumulator ,hashPathPair.second);
      } else {
        collectDuplicatesInAccu(duplicateAccumulator, sizePathsPair.second);        
      }
    }
}

void DuplicateFinder::findPossibleDuplicatesInDir(filepath initialDirectory) {
  qDebug() << "Checking...";
  auto size2possibleDuplicates = collectSameSizeFiles(initialDirectory);
  if ( QThread::currentThread()->isInterruptionRequested() ) {
     return;
  }
  qDebug() << "Checking" << size2possibleDuplicates.size() << "possibilities";
  QAtomicInt counter {};
  auto possibleDuplicateCount = size2possibleDuplicates.size();
  emit this->foundPossibleDuplicates(possibleDuplicateCount);
  auto chunked = iter::grouper(size2possibleDuplicates, std::min(possibleDuplicateCount, 20lu));
  //auto chunked_vector = std::vector<decltype(*chunked.begin())> { chunked.begin(), chunked.end() };  // blocking map needs at least an input iterator, but blockingMap requires at least a forward iterator
  QtConcurrent::blockingMap(chunked.begin(), chunked.end(), [this](decltype(*chunked.begin()) chunk) {
    std::vector< std::vector< QString > > duplicateAccumulator {};
    for (const auto& elem: chunk)
      processElements(duplicateAccumulator, elem);
    model.addMultipleDuplicatedFiles(duplicateAccumulator);
  });
  emit processedAllFiles();
}

DuplicateFinder::DuplicateFinder(DuplicateModel& m, QObject* parent): model(m) {}
