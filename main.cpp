#include <QApplication>
#include <QObject>
#include <QModelIndex>
#include <QDebug>
#include <QFileDialog>
#include <QtConcurrent>
#include <QThread>
#include <QFuture>
#include "duplicatefinder.h"
#include "duplicatemodel.h"
#include "ui_mainwindow.h"
#include "modeltest.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    
    app.setAttribute(Qt::AA_UseHighDpiPixmaps, true);

    QThread worker {};
    
    DuplicateModel dm {};
    DuplicateFinder df {dm};
    df.moveToThread(&worker);
    worker.start();
    
    QFileDialog folderSelectionDialog {};
    folderSelectionDialog.setFileMode(QFileDialog::Directory);
    folderSelectionDialog.setOption(QFileDialog::ShowDirsOnly, true);
    folderSelectionDialog.setDirectory(QDir::homePath());

    QMainWindow *mw = new QMainWindow;
    Ui_MainWindow ui;
    ui.setupUi(mw);

    ui.dupFilesOverview->setModel(&dm);
    ui.dupFilesDetailview->setModel(&dm);
    
    ui.progressBar->hide();
    
    // set up connections
    QObject::connect(ui.folderChooserButton, &QPushButton::clicked, &folderSelectionDialog, &QFileDialog::exec);
    
    QObject::connect(&folderSelectionDialog, &QFileDialog::fileSelected, &df, &DuplicateFinder::findPossibleDuplicatesInDir);
    QObject::connect(&folderSelectionDialog, &QFileDialog::fileSelected, [&ui](){
      ui.progressBar->setRange(0,0);
      ui.progressBar->show();
      ui.folderChooserButton->setEnabled(false);
    });
    
    QObject::connect(ui.dupFilesOverview->selectionModel(), &QItemSelectionModel::selectionChanged, [&ui](const QItemSelection& selection) {
       QModelIndex index  =selection.indexes().first();
       ui.dupFilesDetailview->setRootIndex(index);
    });
    
    QObject::connect(&df, &DuplicateFinder::foundPossibleDuplicates, ui.progressBar, &QProgressBar::setMaximum);
    QObject::connect(&df, &DuplicateFinder::processedDuplicates, ui.progressBar, &QProgressBar::setValue);
    QObject::connect(&df, &DuplicateFinder::processedAllFiles, ui.progressBar, &QProgressBar::hide);
    
    QObject::connect(&app, &QApplication::aboutToQuit, [&worker](){
      worker.requestInterruption();
      worker.quit();
      if (!worker.wait(3000)) {
        worker.terminate();
        worker.wait(1000);
      }
    });
    
    mw->show();
    return app.exec();
}
